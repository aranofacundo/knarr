FROM ubuntu:jammy

LABEL maintainer="aranofacundo@berserker.com.ar" \
    version="2.0.0-alpha1"

ARG S6_OVERLAY_ARCH=x86_64
ARG S6_OVERLAY_VERSION=3.1.5.0
ARG PHP_VERSION=8.0

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8

# Common user and group IDs. Prevents issue when editing files from volumes
ENV PUID 1000
ENV PGID 1000

# Default enviromental vars
ENV ENABLE_CRON 0
ENV ENABLE_CADDY 1
ENV PATH $PATH:/usr/local/bin

# APT install and clean wrapper
COPY rootfs/usr/bin /usr/bin
RUN chmod +x /usr/bin/knarr_install /usr/bin/knarr_upgrade

# Install base dependencies
RUN knarr_upgrade \
    && knarr_install curl bash cron ca-certificates tzdata nano xz-utils gpg gpg-agent debian-keyring debian-archive-keyring apt-transport-https software-properties-common

# Add Caddy repository for Ubuntu/Debian
RUN curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
RUN curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-stable.list

# Add Ondrej PHP repository for Ubuntu
RUN add-apt-repository ppa:ondrej/php

# Install webserver
RUN knarr_install caddy php${PHP_VERSION} php${PHP_VERSION}-fpm php${PHP_VERSION}-mysql php${PHP_VERSION}-curl

# Add S6 Overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

# Set up common user and group
RUN groupadd -g ${PGID} knarr && \
    useradd -u ${PUID} -d /dev/null -s /sbin/nologin -g knarr knarr

# Copy remaining files
COPY rootfs/etc /etc

# Fix exec permission for oneshot service scripts
RUN chmod -R +x /etc/s6-overlay/scripts

# Add WP-CLI
RUN curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/local/bin/wp && chmod +x /usr/local/bin/wp

VOLUME ["/tmp"]
EXPOSE 80 443
ENTRYPOINT ["/init"]
