# Knarr

Una pequeña imagen basada Ubuntu LTS con S6, cron, curl y nginx.

La imagen se encuentra en `registry.gitlab.com/aranofacundo/knarr`

-   `latest` basado en la rama `master`
-   `stable` basado en `tags`

### Variables Ambientales (env)

Estas variables pueden ser configuradas usando Docker (o podman)

```toml
### Servicios
# 0 = Deshabilita servicio
# 1 = Habilita servicio
ENABLE_CRON=0 # Valores por defecto
ENABLE_NGINX=1 # Valores por defecto

### Usuario knarr (WIP)
# Permite configurar el usuario knarr para eliminar privilegios
PUID=1000
PGID=1000
```

### Puertos

-   nginx `80`

### Scripts

-   `knarr_install`: remplaza `apt update`, `apt install` y elimina cache APT en un solo comando.
-   `knarr_upgrade`: remplaza `apt update`, `apt upgrade` y elimina cache APT en un solo comando.
-   `knarr_repo`: agrega repositorios APT y PPA recomendados (nginx, nodesource, php) **(WIP)**.
